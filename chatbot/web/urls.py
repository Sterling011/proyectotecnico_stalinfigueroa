from django.urls import path

from web import views

urlpatterns = [
    path('', views.home, name = 'home'),
    path('consulta',views.answer, name = 'responder'),
    path('api',views.ApiChatbot.as_view(), name = 'Chatbot API'),
    path('answerPLN',views.answerPLN, name='PLN'),
    path('about', views.about, name = 'about')
]
