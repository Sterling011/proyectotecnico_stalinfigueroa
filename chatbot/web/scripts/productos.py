import json
import pandas as pd
import mysql.connector
from mysql.connector import Error

class Productos(object):

    def __init__(self, filename):
        # Leer el archivo json con los productos.
        self.lista1=json.load(open('web/productosFiles/'+filename, encoding='utf-8'))

    def productosToBD(self):
        #Insert de los productos en la BD por porciones del json.
        z = 0
        resultado = True
        for i in range(8):
            query = "INSERT INTO Products (indexitem,code,originalname,name,nameFile,originalingredientLines,ingredientLines,rating,course,cuisine,url,piquant,sour,salty,sweet,bitter,meaty,bag_of_words) VALUES "
            for ord in range(z, round(len(self.lista1['indexitem'])/8)*(i+1)):
                index = str(ord) 
                if ord == round(len(self.lista1['indexitem'])/8)*(i+1)-1:
                    query += f"({self.lista1['indexitem'][index]}, '{self.lista1['code'][index]}', '{self.removerComilla('originalname', index)}', '{self.removerComillaAfter(self.returnString('name', index))}', '{self.lista1['nameFile'][index]}', '{self.removerComillaAfter(self.returnString('originalingredientLines', index))}', '{self.returnString('ingredientLines', index)}', {self.lista1['rating'][index]}, '{self.returnString('attributes.course', index)}', '{self.returnString('attributes.cuisine', index)}', '{self.lista1['attribution.url'][index]}', {'null' if self.lista1['flavors.Piquant'][index] == None else self.lista1['flavors.Piquant'][index]}, {'null' if self.lista1['flavors.Piquant'][index] == None else self.lista1['flavors.Sour'][index]}, {'null' if self.lista1['flavors.Piquant'][index] == None else self.lista1['flavors.Salty'][index]}, {'null' if self.lista1['flavors.Piquant'][index] == None else self.lista1['flavors.Sweet'][index]}, {'null' if self.lista1['flavors.Piquant'][index] == None else self.lista1['flavors.Bitter'][index]}, {'null' if self.lista1['flavors.Piquant'][index] == None else self.lista1['flavors.Bitter'][index]}, '{self.removerComilla('bag_of_words',index)}')"
                else:
                    query += f"({self.lista1['indexitem'][index]}, '{self.lista1['code'][index]}', '{self.removerComilla('originalname', index)}', '{self.removerComillaAfter(self.returnString('name', index))}', '{self.lista1['nameFile'][index]}', '{self.removerComillaAfter(self.returnString('originalingredientLines', index))}', '{self.returnString('ingredientLines', index)}', {self.lista1['rating'][index]}, '{self.returnString('attributes.course', index)}', '{self.returnString('attributes.cuisine', index)}', '{self.lista1['attribution.url'][index]}', {'null' if self.lista1['flavors.Piquant'][index] == None else self.lista1['flavors.Piquant'][index]}, {'null' if self.lista1['flavors.Piquant'][index] == None else self.lista1['flavors.Sour'][index]}, {'null' if self.lista1['flavors.Piquant'][index] == None else self.lista1['flavors.Salty'][index]}, {'null' if self.lista1['flavors.Piquant'][index] == None else self.lista1['flavors.Sweet'][index]}, {'null' if self.lista1['flavors.Piquant'][index] == None else self.lista1['flavors.Bitter'][index]}, {'null' if self.lista1['flavors.Piquant'][index] == None else self.lista1['flavors.Bitter'][index]}, '{self.removerComilla('bag_of_words',index)}'),"
            if not self.addProducts(query):
                resultado = False
            z+=round(len(self.lista1['indexitem'])/8)   
        return resultado

    def removerComilla(self, column, index):
            return self.lista1[column][index].replace("'", "")

    def removerComillaAfter(self, string):
            return string.replace("'", "")    

    def returnString(self, column, index):
            return ', '.join(text for text in self.lista1[column][index])    

    def addProducts(self, stringQuery):
            try:
                connection = mysql.connector.connect(host='localhost', database='tesis', user='root', password='')

                cursor = connection.cursor()
                cursor.execute(stringQuery)
                connection.commit()
                print("Productos insertados correctamente")

                return True

            except mysql.connector.Error as error:
                print("Failed to create table in MySQL: {}".format(error))
                return False

            finally:
                if connection.is_connected():
                    cursor.close()
                    connection.close()
                    print("MySQL connection is closed")    

def productosConsulta(textoProducto):

    listaProductos = []

    try:
        connection = mysql.connector.connect(host='localhost', database='tesis', user='root', password='')

        cursor = connection.cursor()
        query = "SELECT originalname, rating, cuisine FROM products WHERE bag_of_words LIKE %s and rating=5 LIMIT 5"
        cursor.execute(query, ("%"+textoProducto+"%",))
    
        results = cursor.fetchall()
        connection.close()

        for producto in results:
            #print (text[0])
            listaProductos.append({'producto': producto[0], 'rating': producto[1], 'cuisine': producto[2]}) 
        
        return listaProductos

    except mysql.connector.Error as error:
        print("Failed to execute the query in MySQL: {}".format(error))
        return None