'''
INSTALAR LIBRERÍA
    pip install -U spacy
    instalar con python -m spacy download es_core_news_md
INSTALAR LIBRERÍA
'''
import nltk
import string
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.feature_extraction.text import TfidfVectorizer

from web.apps import WebConfig

class modeloPLN(object):     

    def responder(self, textoUsuario):

        def tokenizarTexto(textoEntrada):
            textoEntrada=str(textoEntrada).lower()
            tokenizado = nltk.tokenize.word_tokenize(textoEntrada)
            return tokenizado

        def removerStopWords(textoTokenizado):
            stop_word_set = set(nltk.corpus.stopwords.words("spanish"))
            filteredContents = [word for word in textoTokenizado if word not in stop_word_set]
            return filteredContents

        def lemmatization(textoTokenizado):
            filtradoStemming = []
            for word in WebConfig.sp(' '.join(textoTokenizado)):
                filtradoStemming.append(word.lemma_)
            return filtradoStemming

        def removerPuntuacion(textoTokenizado):
            filtradoPuntuacion=[]
            replacements = (("á", "a"),("é", "e"),("í", "i"),("ó", "o"),("ú", "u"),
                            ("à", "a"),("è", "e"),("ì", "i"),("ò", "o"),("ù", "u"),
                            #("ñ", "n"),
                           )
            signosPuntuacion = set(string.punctuation)
            for word in textoTokenizado:
                if word not in signosPuntuacion:
                    for a, b in replacements:
                        word = word.replace(a, b).replace(a.upper(), b.upper())
                    filtradoPuntuacion.append(word)
            return filtradoPuntuacion

        def procesarTexto(textoEntrada):
            textoProcesado = tokenizarTexto(textoEntrada)
            textoProcesado = removerStopWords(textoProcesado)
            textoProcesado = removerPuntuacion(textoProcesado)
            textoProcesado = lemmatization(textoProcesado)    
            #print(textoProcesado)
            return textoProcesado

        dictionary = {0: {"horario": "El horario de atención es Martes a Viernes desde las 7am hasta las 8pm."},
                      1: {"sucursal direccion ubicacion": "Las sucursales son Av. de las Américas. Av Gil Ramírez Dávalos"},
                      2: {"mejor plato": "El mejor plato es el chaulafan"},
                      3: {"hola saludos nombre": "Hola! Mi nombre es Quickbot"},
                      4: {"quienes somos valores mision": "Somos una empresa bastante cumplida"},
                      5: {"modos de pago metodos de pago": "Los modos de pago aceptados son efectivo y con tarjeta"},
                      6: {"entrega domicilio": "Sí, realizamos entregas a domicilio"}}

        corpus = []

        for p_id, p_info in dictionary.items():   
            for key in p_info:
                corpus.append(key)

        corpus.append(textoUsuario)

        n=len(corpus)

        tfidf = TfidfVectorizer(tokenizer=procesarTexto)

        tfs = tfidf.fit_transform(corpus)

        coseno=np.array(cosine_similarity(tfs, tfs))   

        #print(coseno)

        if coseno[n-1:n,:n-1].max() > 0.1 :
            result = coseno[n-1:n,:n-1].argmax()

            for key, value in dictionary[result].items():
                return(value)
        else:
            return False
    