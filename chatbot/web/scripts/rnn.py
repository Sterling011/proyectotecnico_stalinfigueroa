import os
import pathlib
import pandas as pd
import numpy as np
import pandas as pd
from string import punctuation
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
from keras.layers import Dense, Embedding, LSTM, SpatialDropout1D
from sklearn.model_selection import train_test_split
from keras.utils.np_utils import to_categorical

from keras.models import model_from_json
from keras.models import load_model
from pandas import DataFrame

import nltk
from nltk.stem.wordnet import WordNetLemmatizer

from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
from string import punctuation

import sqlite3
import pickle

from tensorflow.python.keras.models import Sequential

from web.apps import WebConfig

class modeloRNN:

    def request(self,userText):
        #PREDICCIÓN DE SENTIMIENTO Y CERTEZA
        #num_Columnas=X.shape[1]
        twt = [userText]

        print('Tweet',twt)
        #FLUJO DEL CHATBOT
        # Preguntas frecuentes...

        #Dejar comentarios

        #Incrustado en Wordpress

        #Cargar RN cada cierto tiempo...

        #Contenedor x chatbot...

        #vectorizing the tweet by the pre-fitted tokenizer instance
        twt = WebConfig.tokenizer.texts_to_sequences(twt)
        #padding the tweet to have exactly the same shape as `embedding_2` input
        num_Columnas=WebConfig.model.input_shape[1]
        twt = pad_sequences(twt, maxlen=num_Columnas, dtype='int32', value=0)
        respuesta = self.predict(twt)
        return respuesta
    

    def predict(self,twt):
        try:
            sentiment = WebConfig.model.predict(twt,batch_size=1,verbose = 2)[0]
            print(sentiment)
            print(np.argmax(sentiment))
            respuesta=""
            if(np.argmax(sentiment) == 0):
                prob = sentiment[0]
                respuesta = ("%s %.2f%%. %s" % ('No le gustó con certeza del', prob*100, "Su comentario es de gran ayuda para mejorar"))
                print(respuesta)
            elif (np.argmax(sentiment) == 1):
                prob = sentiment[1]
                respuesta = ("%s %.2f%%. %s" % ('Le gustó con certeza del', prob*100, "Gracias por su comentario, seguiremos mejorando para usted"))
                print(respuesta)
            
            return respuesta
        except KeyError:
            respuesta='No comprendo tu comentario'
            print(respuesta)
            return respuesta

    '''
class modeloRNN:

    def __init__(self):
            self.Selectedmodel = Sequential()
            max_fatures = 2000
            self.tokenizer = Tokenizer()

    # Cargar RED NEURONAL
    def cargarRNN(self, nombreArchivoModelo, nombreArchivoPesos):
        print('Directorio: '+ os.getcwd())
        # Cargar el modelo de la red neuronal desde el archivo .h5
        # Cargar Pesos (weights) en el nuevo modelo
        model = load_model('D:\\Documentos\\UPS10\\Tesis\\chatbot\\web\\scripts\\'+nombreArchivoModelo+'.h5')  
        #model.load_weights('D:\\Documentos\\UPS10\\Tesis\\chatbot\\web\\scripts\\'+nombreArchivoPesos+'.h5')  
        print("Red Neuronal cargada desde archivo") 
        return model


    # Cargar TOKENIZER
    def cargarTokenizer(self, nombreArchivoTokenizer):
        with open('D:\\Documentos\\UPS10\\Tesis\\chatbot\\web\\scripts\\'+nombreArchivoTokenizer+'.pickle', 'rb') as handle:
            tokenizer = pickle.load(handle)
        return tokenizer
        

    def request(self,userText):
        #PREDICCIÓN DE SENTIMIENTO Y CERTEZA
        #num_Columnas=X.shape[1]
        twt = [userText]

        print('Tweet',twt)

        #self.Selectedmodel=self.cargarRNN('modeloRNN','pesosRNN')
        #self.Tokenizer=self.cargarTokenizer('tokenizerRNN')
        #vectorizing the tweet by the pre-fitted tokenizer instance
        twt = self.tokenizer.texts_to_sequences(twt)
        #padding the tweet to have exactly the same shape as `embedding_2` input
        twt = pad_sequences(twt, maxlen=None, dtype='int32', value=0)
        #print(twt)
        respuesta = self.predict(twt)
        return respuesta
    

    def predict(self,twt):

        sentiment = self.Selectedmodel.predict(twt,batch_size=1,verbose = 2)[0]
        print(np.argmax(sentiment))
        respuesta=""
        if(np.argmax(sentiment) == 0):
            prob = sentiment[0]
            respuesta = ("%s %.2f%%. %s" % ('No le gustó con certeza del', prob*100, "Muchas gracias por su comentario, lo tomaremos en cuenta para mejorar"))
            print(respuesta)
        elif (np.argmax(sentiment) == 1):
            prob = sentiment[1]
            respuesta = ("%s %.2f%%. %s" % ('Le gustó con certeza del', prob*100, "Que bueno que le haya gustado, seguiremos mejorando para usted"))
            print(respuesta)
        return respuesta
'''