from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from .scripts.pln import modeloPLN
from .models import MensajeChatbot
from .scripts.rnn import modeloRNN
from .scripts.productos import Productos
from .scripts.productos import productosConsulta

import os
import json 

from rest_framework.views import APIView
from rest_framework.response import Response

class ApiChatbot (APIView):
    
    def __init__(self):
        self.object = modeloRNN()
        self.pln = modeloPLN()

    def get(self, request, format=None):
        # Obtener la pregunta del usuario
        textoPregunta = request.GET.get('pregunta')
        textoComentario = request.GET.get('comentario')
        textoProducto = request.GET.get('producto')
        if(textoPregunta is not None):
            #Obtener una respuesta segun PLN
            respuesta=self.pln.responder(textoPregunta)
             # Si se obtiene una respuesta de PLN se retorna esa respuesta.
            if respuesta:
                print ("La respuesta del chatbot es: ", respuesta)
                return Response({'id':1, 'respuesta': respuesta})
            else:
                return Response({'id':0, 'respuesta': 'Lo siento, ¿Por favor puedes repetir tu pregunta?'})
        # Obtener el comentario del usuario
        elif (textoComentario is not None):
            print('Comentario')
            # Llamar al la RN
            respuesta = self.object.request(textoComentario)
            print('Respuesta del modelo', respuesta)
            return Response({'id':2, 'respuesta': respuesta })
        # Obtener la consulta del producto del usuario
        elif (textoProducto is not None):
            resultadoConsulta = productosConsulta(textoProducto)
            if resultadoConsulta is not None:
                return Response({'id':3, 'productos': resultadoConsulta})
            else:
                return Response({'id': 4, 'productos': None})

    # Se almacena el archivo json de los productos.
    def post(self, request, *args, **kwargs):
        saveUploadedFile(request.FILES['producto'])
        productos = Productos(str(request.FILES['producto']))
        if productos.productosToBD():
            return Response({'status': True, 'message': 'Productos cargados correctamente'})
        else:
            return Response({'status': False, 'message': 'Ocurrió un error durante la inserción de los productos'})        

def saveUploadedFile(file):
    with open('web/productosFiles/'+str(file), 'wb+') as destination:
        for chunk in file.chunks():
            destination.write(chunk)







###############################################

def home(request):
    print('Directorio2: ',os.getcwd())
    return render(request,'chatbot/home.html')

def answer(request):
    
    print(textoUser)
    object = modeloRNN()
    respuesta = object.request(textoUser)
    print('Respuesta del modelo', respuesta)
    response = {
        'id': 1,
        'respuesta': respuesta
    }
    print('JSON: ',response)
    return HttpResponse(json.dumps(response), content_type='application/json')
    #return JsonResponse(response, json_dumps_params={'ensure_ascii': False})
    

def answerPLN(request):
    #Obtener del GET
    entradaUsuario = request.GET.get('results')
    print ("El texto de a es: ", entradaUsuario)

    #Obtener una respuesta segun Cosine Similarity
    if entradaUsuario is not None:
        respuesta=responder(entradaUsuario)#Llamar a la funcion de PLN.py
        print ("La respuesta del chatbot es: ", respuesta)

    return HttpResponse(respuesta)

def about(request):
    return render(request,'chatbot/about.html')