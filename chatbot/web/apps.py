from django.apps import AppConfig
from keras.models import load_model
import spacy
import os
import pickle

class WebConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'web'

    sp = spacy.load('es_core_news_md')

    # Load your models and model weights here and these are linked "MODELS" variable that we mentioned in "settings.py"
    print('Directorio: ',os.getcwd())
    #path = os.path.join('', "modeloRNN.h5") 
    path = 'D:\\Documentos\\UPS10\\Tesis\\chatbot\\models\\modeloRNN.h5'
    model = load_model(path)

    #path1 = os.path.join(settings.MODEL_ROOT, "tokenizerRNN.pickle")
    path1 = 'D:\\Documentos\\UPS10\\Tesis\\chatbot\\models\\tokenizerRNN.pickle'
    with open(path1, 'rb') as handle:
         tokenizer = pickle.load(handle)


