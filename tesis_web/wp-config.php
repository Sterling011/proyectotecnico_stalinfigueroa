<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'tesis_web' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ':5R,5Yc ZAKX7B!s;Kbo&%s`?|.oo6[;|lTR90Zw$8j3{w#r2.LI3c%dm=6`(bm<' );
define( 'SECURE_AUTH_KEY',  ';RmYH|P9#`peiyAxE[XB(EL&DmpEP7QG@^z$b*F%?5Tj+u1N<LL.5eHVZ[^~(BOe' );
define( 'LOGGED_IN_KEY',    '^o]Vz){gPSkyYG;lLxmD>%~MfJu2A?pU*;Tupi2luA]W.1s^<SqSh)vQ:j`1_16k' );
define( 'NONCE_KEY',        '}hRKD@.A:;WEbUFghy5e@9?+x>6]s%2dC$.K5tSj3V2uU77ETY@/y(VlFFajwF8a' );
define( 'AUTH_SALT',        '%j9@O(q]Mfd&d!VM[!sQn<2VZA-NZRH- B9uHH  U!&H }@s^TIc=*80%9E?:~}-' );
define( 'SECURE_AUTH_SALT', '>Q-Yig@J~rr7t}oB!IbWD78)sjt,iON]w?vS|9{-N25RUhjZZJlWA5,/MlJkf fE' );
define( 'LOGGED_IN_SALT',   'Jtc;Fc=`NQda~V`?ZN,QjlEj)+Uh0]P[vlUye3OYG#DdU>g:+w[T 5Y=E@Ab!Q8R' );
define( 'NONCE_SALT',       '#OOnV*,`SdWX[eC*iX-6i50X2f&2ue6Sh%>!Xar[;,BrABIuqjJU#CWN%N2Xx/Lo' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
