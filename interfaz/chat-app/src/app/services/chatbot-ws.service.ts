import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class ChatbotWSService {

  constructor(private http:HttpClient) { }

  public getResponsePregunta(message:any){
    console.log('Entra service')
    let header = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    return this.http.get("/app/api?pregunta="+message,{headers:header})
  }

  public getResponseComentario(message:any){
    console.log('Entra service')
    let header = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    return this.http.get("/app/api?comentario="+message,{headers:header})
  }

  public getResponseProducto(producto: any){
    console.log('Entra service producto')
    let header = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    return this.http.get("/app/api?producto="+producto,{headers:header})
  }

}
