import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProductosWSService {

  constructor(private http:HttpClient) { }

  public getResponseProducto(fileProductos: any){
    let header = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    return this.http.post('/app/api/productos', fileProductos,{headers:header}).subscribe(
      data => console.log(data),
      error => console.log(error)
    );
  }
}
