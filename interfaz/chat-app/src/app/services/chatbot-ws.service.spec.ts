import { TestBed } from '@angular/core/testing';

import { ChatbotWSService } from './chatbot-ws.service';

describe('ChatbotWSService', () => {
  let service: ChatbotWSService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ChatbotWSService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
