import { TestBed } from '@angular/core/testing';

import { ProductosWSService } from './productos-ws.service';

describe('ProductosWSService', () => {
  let service: ProductosWSService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProductosWSService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
