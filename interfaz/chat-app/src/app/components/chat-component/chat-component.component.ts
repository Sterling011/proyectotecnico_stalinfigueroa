import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient, JsonpClientBackend } from '@angular/common/http';
import { DatePipe } from '@angular/common'
import { v4 } from 'uuid';

import {ChatbotWSService} from '../../services/chatbot-ws.service'

import { Mensaje, MensajeProducto, Productos} from '../../intefaces/Mensaje';
import { NavController} from '@ionic/angular';

@Component({
  selector: 'app-chat-component',
  templateUrl: './chat-component.component.html',
  styleUrls: ['./chat-component.component.scss'],
  providers: [ChatbotWSService]
})

export class ChatComponentComponent implements OnInit {

  constructor(private http: HttpClient, private webServiceChatbot:ChatbotWSService, public datePipe: DatePipe, public navCtrl: NavController) { }

  //Array de mensajes Usuario/Bot
  mensajes: Array<Mensaje> = [];
  //Array de menus
  menuOpciones: Array<any> = [];
  //Mensaje que envía el usuario
  mensaje: string = '';
  //Id del mensaje UID
  idUltimoMensaje: any;
  //Hora del mensaje
  time: Date;
  //Estado del mensaje
  msgComentario: Boolean = false;
  msgProducto: Boolean = false;

  ngOnInit() {
    // Comprobar si hay algo almacenado
    if(sessionStorage.length > 0){

      this.msgComentario = JSON.parse(sessionStorage.getItem('msgComentario'))
      this.msgProducto = JSON.parse(sessionStorage.getItem('msgProducto'))

      console.log('Comentarios?: ', this.msgComentario)

      this.menuOpciones = ['Conocer las sucursales',
      'Conocer el horario de atención', 
      'Dejar un comentario general',
      ('Consultar un producto')]

      let msg = JSON.parse(sessionStorage.getItem('mensajes'))
      
      console.log(msg)

      var outputArray = [];  
      for (let element in msg) {  
        let mensaje = msg[element]

        if(mensaje['type'] =='chatbotProducto'){
          const msgProducto : MensajeProducto[] = [{
            id: v4(),
            respuesta: mensaje['respuesta'],
            rating: mensaje['rating'],
            cuisine: mensaje['cuisine'],
            hora: mensaje['hora'],
            type: mensaje['type'],
          }];
          //Agregar el mensaje que envía el usuario a la lista de mensajes.
          this.mensajes = this.mensajes.concat(msgProducto);
        }else{
          const msgChat : Mensaje[] = [{
            id: mensaje['id'],
            respuesta: mensaje['respuesta'],
            hora: mensaje['hora'],
            type: mensaje['type'],
          }];
          //Agregar el mensaje que envía el usuario a la lista de mensajes.
          this.mensajes = this.mensajes.concat(msgChat);
        }
      }
    }else{
      // Cuando se abre el chat, al inicio se carga el menú de bienvenida
      this.show_menuGeneral('inicio')
    }
    
  }

  private enviarMensaje() {  
    // Eliminar los espacios en blanco del mensaje
    this.mensaje = this.mensaje.trim()
    if (this.mensaje !== '') {
      //Añadir el mensaje de usuario a la interfaz del chat.
      this.addMsgUsuario(this.mensaje);
      //Mostrar el menú si el usuario escribe la palabre menu o menú
      if(this.mensaje.toLowerCase() === 'menu' || this.mensaje.toLowerCase() === 'menú'){
        // No recibir palabras clave como comentarios.
        this.msgProducto = false;
        this.msgComentario = false;
        this.show_menuGeneral('normal')
        // Almacenar localmente el estado de la aplicación
        sessionStorage.setItem('msgComentario', JSON.stringify(this.msgComentario))
        sessionStorage.setItem('msgProducto', JSON.stringify(this.msgProducto))        
      }
      //Recibir el comentario del usuario y no cambiar de estado hasta que el comentario sea realizado
      else if(this.msgComentario && !this.msgProducto){
        this.addMsgChatbotComentario(this.mensaje);
        this.msgComentario = false;
        // Almacenar localmente el estado de la aplicación
        sessionStorage.setItem('msgComentario', JSON.stringify(this.msgComentario))
        sessionStorage.setItem('msgProducto', JSON.stringify(this.msgProducto))
      }
      //Recibir el nombre del producto y no cambiar el estado hasta recibir una palabra
      else if(this.msgProducto && !this.msgComentario){
        this.addMsgProducto(this.mensaje);
        this.msgProducto = false;
        // Almacenar localmente el estado de la aplicación
        sessionStorage.setItem('msgComentario', JSON.stringify(this.msgComentario))
        sessionStorage.setItem('msgProducto', JSON.stringify(this.msgProducto))
      }
      // Si no se consulta ninguna de estas palabras se utiliza el chatbot para las preguntas.
      else{
        this.addMsgChatbotPregunta(this.mensaje);
        this.msgComentario = false;       
        // Almacenar localmente el estado de la aplicación
        sessionStorage.setItem('msgComentario', JSON.stringify(this.msgComentario))
        sessionStorage.setItem('msgProducto', JSON.stringify(this.msgProducto))
      }
      //Limpiar el input.
      this.mensaje = '';
    }
  }
      
  //Obtener el tipo de mensaje enviado por el usuario o recibido desde el chatbot.
  private getTipoMensaje(mensajeType: any) {
    return {
      incoming: mensajeType === 'chatbot' || 'chatbotMenuinicio' || 'chatbotMenunormal' || 'chatbotProducto',
      outgoing: mensajeType === 'usuario',
    };
  }

  /* Funcionamiento del menú */

  // Para mostrar el menú se debe identificar si es el menú inicial o es un menú normal.
  // El menú de inicio muestra un saludo y a continuación las opciones
  // El menú normal muestra solamente las opciones.
  private show_menuGeneral(tipoMenu: string){
    this.menuOpciones = []
    var textoMenu = {
      'inicio': "¡Hola! Soy Quickbot, tu Asistente Virtual 👋 para comenzar elige una opción del menú.",
      'normal': "Elige una opción del menú a continuación ⬇️",
      3: 6
    };
    const msgMenu : Mensaje[] = [{
      id: v4(),
      respuesta: textoMenu[tipoMenu],
      hora: this.datePipe.transform(new Date(), 'shortTime'),
      type: "chatbotMenu"+tipoMenu,
    }];
    // Añadir las opciones al array de opciones del menú
    this.menuOpciones.push('Conocer las sucursales');
    this.menuOpciones.push('Conocer el horario de atención');
    this.menuOpciones.push('Dejar un comentario general');
    this.menuOpciones.push('Consultar un producto');
    // Agregar la respuesta de inicio sobre los productos.
    this.mensajes = this.mensajes.concat(msgMenu); 
    // Almacenar localmente el estado de la aplicación
    sessionStorage.setItem('mensajes', JSON.stringify(this.mensajes))
  }

  /* 
  Las opciones del menú cumplen ciertas funciones. Estas opciones son enviadas al CHATBOT
  de manera que retorne una respuesta luego de haber realizado el procesamiento
  mediante PLN o RNN.
  */

  private onActionMenu(accion: any){

    if(accion === 'Dejar un comentario general'){
      this.mostrarOpcionMenu('Dejar un comentario general')
      this.mostrarMsgComentario()
    }
    else if(accion === 'Consultar un producto'){
      this.mostrarOpcionMenu('Consultar un producto')
      this.mostrarMsgProducto()
    }else{
      this.mostrarOpcionMenu(accion)
      this.msgProducto = false
      this.msgComentario = false
      // Almacenar localmente el estado de la aplicación
      sessionStorage.setItem('msgComentario', JSON.stringify(this.msgComentario))
      sessionStorage.setItem('msgProducto', JSON.stringify(this.msgProducto))
      this.webServiceChatbot.getResponsePregunta(accion).subscribe((response: Mensaje) => {
        const message = {
          ...response,
          hora: this.datePipe.transform(new Date(), 'shortTime'),
          type: 'chatbot',
        };
        // Agregar el mensaje que devuelve el chatbot a la cola de mensajes.
        this.mensajes = this.mensajes.concat(message);         
        // Almacenar localmente el estado de la aplicación
        sessionStorage.setItem('mensajes', JSON.stringify(this.mensajes))
      });
    }
  }

  // Construir el mensaje del usuario y agregar a la lista de mensajes.
  private addMsgUsuario(msg: string){
    // Mensaje que envía el usuario - input del usuario
    const msgInputUsuario : Mensaje[] = [{
      id: v4(),
      respuesta: msg,
      hora: this.datePipe.transform(new Date(), 'shortTime'),
      type: "usuario",
    }];
    //Agregar el mensaje que envía el usuario a la lista de mensajes.
    this.mensajes = this.mensajes.concat(msgInputUsuario);
    // Almacenar localmente el estado de la aplicación
    sessionStorage.setItem('mensajes', JSON.stringify(this.mensajes))
  }

  // Construir el mensaje con la respuesta del chatbot para una pregunta y agregar a la lista de mensajes.
  private addMsgChatbotPregunta(msg: string){
    //HABILITAR con el chatbot
    this.webServiceChatbot.getResponsePregunta(msg).subscribe((response: Mensaje) => {
      const message = {
        ...response,
        hora: this.datePipe.transform(new Date(), 'shortTime'),
        type: 'chatbot',
      };
      // Agregar el mensaje que devuelve el chatbot a la cola de mensajes.
      this.mensajes = this.mensajes.concat(message);         
      // Almacenar localmente el estado de la aplicación
      sessionStorage.setItem('mensajes', JSON.stringify(this.mensajes))
    });
    //HABILITAR con el chatbot */
  }

  // Construir el mensaje con la respuesta del chatbot para un comentario y agregar a la lista de mensajes.
  private addMsgChatbotComentario(msg: string){
    //HABILITAR con el chatbot
    this.webServiceChatbot.getResponseComentario(msg).subscribe((response: Mensaje) => {
      const message = {
        ...response,
        hora: this.datePipe.transform(new Date(), 'shortTime'),
        type: 'chatbot',
      };
      // Agregar el mensaje que devuelve el chatbot a la cola de mensajes.
      this.mensajes = this.mensajes.concat(message);         
      // Almacenar localmente el estado de la aplicación
      sessionStorage.setItem('mensajes', JSON.stringify(this.mensajes))
    });
    //HABILITAR con el chatbot */
  }

  

  // Obtener los productos de la consulta realizada por el usuario y construir el mensaje
  // de respuesta del chatbot por cada uno de los productos. 
  private addMsgProducto(producto: string){
    this.webServiceChatbot.getResponseProducto(producto).subscribe((response: Productos) => {
      if(response.productos == null){
        const msgResultadoProducto : Mensaje[] = [{
          id: this.idUltimoMensaje,
          respuesta: 'Ha ocurrido un error, por favor inténtelo de nuevo más tarde.',
          hora: this.datePipe.transform(new Date(), 'shortTime'),
          type: "chatbot",
        }];
        // Agregar el mensaje a la lista de mensajes.
        this.mensajes = this.mensajes.concat(msgResultadoProducto);
        // Almacenar localmente el estado de la aplicación
        sessionStorage.setItem('mensajes', JSON.stringify(this.mensajes))
      }else{
        // Si el servicio web devuelve por lo menos un producto.
        if(response.productos.length > 0){
          // Mostrar un mensaje indicando que si se encontraron productos.
          const msgResultadoProducto : Mensaje[] = [{
            id: v4(),
            respuesta: 'Los productos similares (Top 5) a ' +producto+ ' son:',
            hora: this.datePipe.transform(new Date(), 'shortTime'),
            type: "chatbot",
          }];
          // Agregar el mensaje a la lista de mensajes.
          this.mensajes = this.mensajes.concat(msgResultadoProducto);
          
          // Por cada producto, agregar a la lista de mensajes.
          response.productos.forEach(producto => {
            const msgProducto : MensajeProducto[] = [{
              id: v4(),
              respuesta: producto.producto,
              rating: producto.rating,
              cuisine: producto.cuisine,
              hora: this.datePipe.transform(new Date(), 'shortTime'),
              type: "chatbotProducto",
            }];
            //Agregar el producto a la lista de mensajes.
            this.mensajes = this.mensajes.concat(msgProducto);
            // Almacenar localmente el estado de la aplicación
            sessionStorage.setItem('mensajes', JSON.stringify(this.mensajes))
          });
        }
        else{
          const msgResultadoProducto : Mensaje[] = [{
            id: v4(),
            respuesta: 'No existen productos similares a ' +producto,
            hora: this.datePipe.transform(new Date(), 'shortTime'),
            type: "chatbot",
          }];
          // Agregar la respuesta de inicio sobre los productos.
          this.mensajes = this.mensajes.concat(msgResultadoProducto);
          // Almacenar localmente el estado de la aplicación
          sessionStorage.setItem('mensajes', JSON.stringify(this.mensajes))
        }
      }
    });
  }


  private mostrarOpcionMenu(texto: string){
    const msgInputUsuario : Mensaje[] = [{
      id: v4(),
      respuesta: texto,
      hora: this.datePipe.transform(new Date(), 'shortTime'),
      type: "usuario",
    }];
    //Agregar el mensaje que envía el usuario a la lista de mensajes.
    this.mensajes = this.mensajes.concat(msgInputUsuario);
    // Almacenar localmente el estado de la aplicación
    sessionStorage.setItem('mensajes', JSON.stringify(this.mensajes))
  }

  private mostrarMsgComentario(){
    const msgMenu : Mensaje[] = [{
      id: v4(),
      respuesta: 'Escribe tu comentario a continuación.',
      hora: this.datePipe.transform(new Date(), 'shortTime'),
      type: "chatbot",
    }];
    // Agregar la respuesta de inicio sobre los productos.
    this.mensajes = this.mensajes.concat(msgMenu);
    // Almacenar localmente el estado de la aplicación
    sessionStorage.setItem('mensajes', JSON.stringify(this.mensajes))
    this.msgProducto = false
    this.msgComentario = true
    // Almacenar localmente el estado de la aplicación
    sessionStorage.setItem('msgComentario', JSON.stringify(this.msgComentario))
    sessionStorage.setItem('msgProducto', JSON.stringify(this.msgProducto))
  }

  private mostrarMsgProducto(){
    const msgMenu : Mensaje[] = [{
      id: v4(),
      respuesta: 'Escribe el nombre del producto que deseas consultar.',
      hora: this.datePipe.transform(new Date(), 'shortTime'),
      type: "chatbot",
    }];
    // Agregar la respuesta de inicio sobre los productos.
    this.mensajes = this.mensajes.concat(msgMenu);
    // Almacenar localmente el estado de la aplicación
    sessionStorage.setItem('mensajes', JSON.stringify(this.mensajes))
    this.msgProducto = true
    this.msgComentario = false
    // Almacenar localmente el estado de la aplicación
    sessionStorage.setItem('msgComentario', JSON.stringify(this.msgComentario))
    sessionStorage.setItem('msgProducto', JSON.stringify(this.msgProducto))
  }
}






