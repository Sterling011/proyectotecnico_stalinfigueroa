import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ProductosWSService } from 'src/app/services/productos-ws.service';

@Component({
  selector: 'app-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.scss'],
  providers: [ProductosWSService]
})
export class UploadFileComponent implements OnInit {

  file: File;
  click : boolean = true;
  subido: string;

  constructor(private http: HttpClient, private webServiceProductos: ProductosWSService)  { }

  onFileChanged(event: any) {
    this.file = event.target.files[0];
    this.click = !this.click;
  }

  newProducto() {
    this.click = !this.click;
    const uploadData = new FormData();
    uploadData.append('producto', this.file, this.file.name);

    this.http.post('/app/api', uploadData, {responseType: 'json'}).subscribe((response: any) => {
      console.log(response)
      if (response['status'] == true){
        this.subido = 'true'
      }else{
        this.subido = 'false'
      }
    });

  }

  ngOnInit() {}

}
