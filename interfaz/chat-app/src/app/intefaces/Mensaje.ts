export interface Mensaje {
    id: string;
    respuesta: string;
    hora: string;
    type: string;
}

export interface MensajeProducto {
    id: string;
    respuesta: string;
    rating: string;
    cuisine: string;
    hora: string;
    type: string;
}

export interface Productos{
    id: string;
    productos: Array<Producto>;
    hora: string;
    type: string;
}

export interface Producto{
    producto: string;
    rating: string;
    cuisine: string
}